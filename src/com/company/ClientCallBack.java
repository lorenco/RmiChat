package com.company;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ClientCallBack extends UnicastRemoteObject implements CallBack {
    public ClientCallBack() throws RemoteException {
        super ();
    }

    @Override
    public void communicate(String nick, String text) throws RemoteException {
        System.out.println ( " messages received: " + text );
    }
}
