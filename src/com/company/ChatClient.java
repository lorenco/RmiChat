package com.company;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;
import java.util.Vector;

public class ChatClient {
    private Scanner userInput = new Scanner ( System.in );
    private String nick;
    private ChatService chatService;

    public ChatClient(String hostName) {
        System.out.println ( "Enter client name: " );
        if (userInput.hasNextLine ()) {
            nick = userInput.nextLine ();
        }
        Registry registry;
        try {
            registry = LocateRegistry.getRegistry ( hostName );

            chatService = (ChatService) registry.lookup ( "ChatServer" );

            CallBack callBack = new ClientCallBack ();

            chatService.register ( nick, callBack );
            loop ();
            chatService.unregister ( nick );

        } catch (RemoteException e) {
            e.printStackTrace ();
        } catch (NotBoundException e) {
            e.printStackTrace ();
        }
    }

    private void loop() {
        while (true) {
            String line;
            System.out.println ( "[i] inform  [c] communicate" );
            if (userInput.hasNextLine ()) {
                line = userInput.nextLine ();
                if (!line.matches ( "[ic]" )) {
                    System.out.println ( "You entered invalid sign" );
                    continue;
                }
                switch (line) {
                    case "i":
                        inform ();
                        break;
                    case "c":
                        communicate ();
                        break;
                }
            }
        }
    }

    private void communicate() {
        String line;
        System.out.println ( "Enter the user filter regular expression" );
        if (userInput.hasNextLine ()) {
            line = userInput.nextLine ();
            try {
                chatService.communicate ( nick, line );
            } catch (RemoteException e) {
                e.printStackTrace ();
            }
        }
    }

    private void inform() {
        String line;
        System.out.println ( "Enter the user filter regular expression" );
        if (userInput.hasNextLine ()) {
            line = userInput.nextLine ();
            Vector<String> vector = null;
            try {
                vector = chatService.inform ( line );
            } catch (RemoteException e) {
                e.printStackTrace ();
            }
        }
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println ( "Usage : ChatClient <server host name> error" );
            System.exit ( -1 );
        }
        new ChatClient ( args[0] );
    }
}
