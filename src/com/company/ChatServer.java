package com.company;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

public class ChatServer extends UnicastRemoteObject implements ChatService {

    private Map<String, CallBack> present = new HashMap<> ();

    public ChatServer() throws RemoteException {
    }

    @Override
    public boolean register(String nick, CallBack callBack) throws RemoteException {
        System.out.println ( "Server.register(): " + nick );
        if (!present.containsKey ( nick )) {
            present.put ( nick, callBack );
            return true;
        }
        return false;
    }

    @Override
    public boolean unregister(String nick) throws RemoteException {
        if (present.remove ( nick ) != null) {
            System.out.println ( "Server.unregister(): " + nick );
            return true;
        }
        return false;
    }

    @Override
    public Vector<String> inform(String nick) throws RemoteException {
        Set<String> stringSet = present.keySet ();
        Vector<String> vector = new Vector<> ();
        for (String s : stringSet) {
            if (s.matches ( nick )) {
                return vector;
            }
        }
        return null;
    }

    @Override
    public boolean communicate(String nick, String text) throws RemoteException {
        System.out.println ( "Server.communicate(): " + text );
        CallBack callBack = present.get ( nick );
        if (callBack != null) {
            callBack.communicate ( nick, text );
            return true;
        }
        return false;
    }
}
