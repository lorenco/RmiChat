package com.company;

import java.rmi.RemoteException;

public interface CallBack {
    public void communicate(String nick, String text) throws RemoteException;
}
